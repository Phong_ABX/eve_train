import logging as logger
from eve import Eve
from flask import request
from eve.methods.get import get
from eve.render import send_response
from flask import  Response
from eve.methods.post import post_internal

from settings import settings

app = Eve(settings  = settings)
mongo = app.data.driver

@app.route("/memory/last_sum", methods=['GET', 'POST'])
def get_field():
    if request.method == "GET":
        field = 'sum'
        # list_res = [ (((get(field))[0])['_items'])[-1]]
        # dict_res = {'_items': list_res}    
        # tuple_res = (dict_res,dict())
        # logger.warning(tuple_res)
        # logger.warning(get(field))
        return send_response(field, get(field) )
    elif request.method == "POST":  
        field = 'sum'
        n = request.form.get("n", 0, int)
        
        list_res = [ (((get(field))[0])['_items'])[-n]]
        dict_res = {'_items': list_res}    
        tuple_res = (dict_res,dict())
        return  send_response(field, tuple_res )


@app.route("/memory/last_sum_object", methods=['GET', 'POST'])
def last_sum_object():
    if request.method == "GET":
        field = 'sum_object'
        
        list_res = [ (((get(field))[0])['_items'])[-3:-1]]
        dict_res = {'_items': list_res}    
        tuple_res = (dict_res,dict())
        logger.warning(tuple_res)
        logger.warning(get(field))
        return send_response(field, get(field) )
    elif request.method == "POST":
        field = 'sum_object'
        n = request.form.get("n", 0, int)
        
        list_res = [ (((get(field))[0])['_items'])[-n]]
        dict_res = {'_items': list_res}    
        tuple_res = (dict_res,dict())
        return  send_response(field, tuple_res )

# @app.route("/function/sum_object", methods=['GET', 'POST'])
# def sum_object():
#     if request.method == "GET":
#         return "hello sum object"
#     elif request.method == "POST":
#         a = request.form.get("a")
        
#         b = request.form.get("b")
#         check = 1

#         try:
#             if a == 'true':
#                 a = 1
#             elif (a == 'false'):
#                 a = 0
#             else: a = float(a)     
#             if request.form.get("a").find('.')<0:
#                 a = int(a)

#             if b == 'true':
#                 b = 1 
#             elif (b == 'false'):
#                 b = 0
#             else: b = float(b)

#             if request.form.get("b").find('.')<0:
#                 b = int(b)
            
#         except:
#             logger.warning(a)  
#             a = str(a)
#             b = str(b)
#             check = 0

#         result = a + b
#         return "%s" % (
#              result
#         )
@app.route("/function/sum/", methods=['GET', 'POST'])
def sum():
    if request.method == "GET":
        return "hello sum"
    elif request.method == "POST":
        check = 1
        a = request.form.get("a", 0, float)
        if request.form.get("a").find('.')>0:
            check = 0 
            logger.warning('1')

        b = request.form.get("b", 0, float)
        
        if request.form.get("b").find('.')>0:
            check = 0
            logger.warning('2') 

        if check:   
            result = int(a + b)
        else:
            result = a + b
        payload = {
            "a": a,
            "b": b,
            "result": result
            }
        with app.test_request_context():
             x = post_internal('sum', payload)
        return "%s + %s = %s" % (
            a, b, result
        )
if __name__ == '__main__':
    app.run(host="0.0.0.0",port=7789)
