from eve import Eve
import importlib
from collections import OrderedDict

# from .. import config, constants
settings = {
    'MONGO_HOST': 'localhost',
    'MONGO_PORT': 27017,
    'MONGO_DBNAME': 'DB',
    'DOMAIN': {
        'sum': {
            'url': 'memory/function/sum',
            'resource_methods': ["GET", "POST"],
            'schema': {
                'a': {
                    'type': 'string',
                },
                'b': {
                    'type': 'string',
                },
                'result': {
                    'type': 'string',
                },
            },
        },
        # 'sum_object': {
        #     'url': 'memory/function/sum_object',
        #     'resource_methods': ["GET", "POST"],
        #     'schema': {
        #         'a': {
        #             'type': 'string',
        #         },
        #         'b': {
        #             'type': 'string',
        #         },
        #         'result': {
        #             'type': 'string',
        #         },
        #     },
        # },


     },
}